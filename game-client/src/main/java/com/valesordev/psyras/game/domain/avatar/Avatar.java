/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.domain.avatar;

/**
 * This is the representation of the player in the game.
 * <p>
 * Created by bashburn on 9/19/15.
 */
public class Avatar {
  private final static int LOC_X = 0;
  private final static int LOC_Y = 1;
  private final static int LOC_Z = 2;
  private final String name;
  private double[] location = new double[3];

  public Avatar(final String name) {
    this.name = name;
  }

  public double[] getLocation() {
    return location;
  }

  public void setLocation(final double[] location) {
    this.location = location;
  }

  public String getName() {
    return name;
  }
}
