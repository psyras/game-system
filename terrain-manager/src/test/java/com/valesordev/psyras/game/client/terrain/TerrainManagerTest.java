/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain;

import com.valesordev.psyras.game.client.terrain.area.TerrainArea;
import com.valesordev.psyras.game.client.terrain.area.TerrainAreaDefinition;
import com.valesordev.psyras.game.client.terrain.math.Point;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * Testing the terrain manager to make sure that the Terrain Areas are loaded correctly.
 * <p>
 * Created by bashburn on 9/19/15.
 */
public class TerrainManagerTest {

  private ConfigurationManager configurationManager;

  @Before
  public void setUp() throws Exception {
    configurationManager = mock(ConfigurationManager.class);
    TerrainAreaDefinition definition = new TerrainAreaDefinition(17, 1024);
    when(configurationManager.loadDefinition(anyString(), eq(TerrainAreaDefinition.class))).thenReturn(definition);
  }

  @Test
  public void testLoadTerrainArea() throws Exception {
    TerrainManager tm = new TerrainManager(configurationManager, () -> new double[]{0d, 0d, 0d});
    TerrainArea ta = tm.loadTerrainArea(new Point(0d, 0d, 0d));
    assertThat(ta, notNullValue());
    assertThat(ta.getSize(), greaterThan(0));
  }
}