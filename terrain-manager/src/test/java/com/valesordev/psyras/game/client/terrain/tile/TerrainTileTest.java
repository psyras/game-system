/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.tile;

import com.valesordev.psyras.game.client.terrain.resources.AbstractResourceManager;
import com.valesordev.psyras.game.client.terrain.resources.DefaultResourceManager;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * We need to verify that terrain tiles are loaded properly and they contain all the data necessary.
 * Created by bashburn on 9/23/15.
 */
public class TerrainTileTest {
  @Test
  public void verifyHeightMap() {
    AbstractResourceManager resourceManager = new DefaultResourceManager();
    TerrainTileDefinition tileDefinition = new TerrainTileDefinition();
    tileDefinition.setName("test-area");
    tileDefinition.setLocation(0);
    tileDefinition.setSize(1024);
    TerrainTile tile = new TerrainTile(resourceManager, tileDefinition);
    float[] heightmap = tile.getHeightmap();
    assertThat(heightmap, notNullValue());
    int length = (tileDefinition.getSize() - 1) + ((tileDefinition.getSize() - 1) * tileDefinition.getSize()) + 1;
    assertThat(heightmap.length, is(length));
  }
}
