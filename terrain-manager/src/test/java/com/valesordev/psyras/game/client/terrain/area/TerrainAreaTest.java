/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.area;

import com.valesordev.psyras.game.client.terrain.ConfigurationManager;
import org.junit.Before;
import org.junit.Test;

/**
 * Test the loading of a test terrain area.
 * <p>
 * Created by bashburn on 9/19/15.
 */
public class TerrainAreaTest {

  private ConfigurationManager configurationManager;

  @Before
  public void setUp() throws Exception {
    configurationManager = ConfigurationManager.getInstance();
  }

  @Test
  public void testLoadedTiles() throws Exception {
//    TerrainArea area = new TerrainArea(configurationManager, "test-area", () -> new double[]{0, 0, 0});
//    ImmutableList<TerrainTile> tiles = area.getLoadedTiles();
//    assertThat(tiles, notNullValue());
//    assertThat(tiles.size(), is(9));
//    assertThat(tiles.get(4).getLocation(), is(7));
//    assertThat(tiles.get(3).getLocation(), is(6));
//    assertThat(tiles.get(5).getLocation(), is(8));
  }
}