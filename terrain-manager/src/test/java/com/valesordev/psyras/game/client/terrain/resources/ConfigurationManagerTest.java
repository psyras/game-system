/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.resources;

import com.valesordev.psyras.game.client.terrain.ConfigurationManager;
import com.valesordev.psyras.game.client.terrain.area.TerrainAreaDefinition;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by bashburn on 9/22/15.
 */
public class ConfigurationManagerTest {

  @Test
  public void testLoadDefinition() throws Exception {
    ConfigurationManager configurationManager = ConfigurationManager.getInstance();
    TerrainAreaDefinition definition = configurationManager.loadDefinition("test-area", TerrainAreaDefinition.class);
    assertThat(definition, notNullValue());
  }
}