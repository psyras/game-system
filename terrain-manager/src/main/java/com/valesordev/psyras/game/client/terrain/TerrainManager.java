/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain;

import com.valesordev.psyras.game.client.terrain.area.TerrainArea;
import com.valesordev.psyras.game.client.terrain.math.Point;

import java.util.function.Supplier;

/**
 * The Terrain Manager handles loading terrain resources, unloading, scaling, and merging each terrain tile.
 * <p>
 * Created by bashburn on 9/19/15.
 */
public class TerrainManager {
  private final ConfigurationManager configurationManager;
  private final Supplier<double[]> avatarLocation;

  public TerrainManager(final ConfigurationManager configurationManager, Supplier<double[]> avatarLocation) {
    this.configurationManager = configurationManager;
    this.avatarLocation = avatarLocation;
  }

  public TerrainArea loadTerrainArea(Point playerLocation) throws Exception {
    String areaName = determineAreaName(playerLocation);
    return new TerrainArea(configurationManager, areaName, avatarLocation);
  }

  private String determineAreaName(final Point playerLocation) {
    return null;
  }
}
