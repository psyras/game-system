/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.tile;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Optional;

/**
 * Created by bashburn on 9/20/15.
 */
public class TileCache {
  private final HashMap<Integer, SoftReference<TerrainTile>> cache = new HashMap<>();

  public TileCache() {
  }

  public Optional<TerrainTile> get(Integer key) {
    SoftReference<TerrainTile> ref = cache.get(key);
    if(ref == null || ref.get() == null) {
      return Optional.empty();
    } else {
      return Optional.of(ref.get());
    }
  }

  public void put(Integer key, TerrainTile value) {
    cache.put(key, new SoftReference<>(value));
  }
}
