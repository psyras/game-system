/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.area;

import com.valesordev.psyras.game.client.terrain.tile.TerrainTileDefinition;

import java.util.Map;

/**
 * Created by bashburn on 9/19/15.
 */
public class TerrainAreaDefinition {
  private int size;
  private int cacheSize = 9;
  private int factor;
  private Map<Integer, TerrainTileDefinition> tileDefinitions;

  public TerrainAreaDefinition() {
  }

  public TerrainAreaDefinition(int size, int factor) {
    this.size = size;
    this.factor = factor;
  }

  public TerrainTileDefinition getTileDefinition(final int index) {
    return tileDefinitions.get(index);
  }

  public void setTileDefinitions(final Map<Integer, TerrainTileDefinition> tileDefinitions) {
    this.tileDefinitions = tileDefinitions;
  }

  public int getCacheSize() {
    return cacheSize;
  }

  public int getFactor() {
    return factor;
  }

  public int getSize() {
    return size;
  }

  public void setSize(final int size) {
    this.size = size;
  }
}
