/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.area;

import com.google.common.collect.ImmutableList;
import com.valesordev.psyras.game.client.terrain.ConfigurationManager;
import com.valesordev.psyras.game.client.terrain.DefinitionException;
import com.valesordev.psyras.game.client.terrain.tile.TerrainTile;
import com.valesordev.psyras.game.client.terrain.tile.TileCache;
import com.valesordev.psyras.utilities.CustomCollectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * The Terrain Area represents an area of terrain that is connected together.
 * <p>
 * Created by bashburn on 9/19/15.
 */
public class TerrainArea {
  private final TileCache cache = new TileCache();
  private final ConfigurationManager configManager;
  private final TerrainAreaDefinition areaDefinition;
  private final List<TerrainTile> loadedTiles;
  private final Supplier<double[]> location;
  private double[] lastLocation;

  public TerrainArea(final ConfigurationManager configManager, final String areaName, final Supplier<double[]> location)
      throws DefinitionException {
    this.configManager = configManager;
    this.areaDefinition = configManager.loadDefinition(areaName, TerrainAreaDefinition.class);
    this.location = location;
    this.lastLocation = location.get();
    if(lastLocation.length != 3) throw new IllegalArgumentException("Location must supply 3 doubles");
    this.loadedTiles = new ArrayList<>();
    this.lastLocation = new double[]{Double.NaN, Double.NaN, Double.NaN};
  }

  private int convertToIndex(final double[] location) {
    int size = getFactor();
    int x = (int)Math.round(location[0] / size);
    int z = (int)Math.round(location[2] / size);
    return Math.round(x + (z * getSize())) + ((getSize() - 1) / 2);
  }

  private TerrainTile createTerrainTile(final int index) {
    if(index < 0) throw new IllegalArgumentException("Terrain Tile index must be greater than 0");
    if(index > ((getSize() - 1) + ((getSize() - 1) * getSize())))
      throw new IllegalArgumentException("Terrain Tile index must be less than the total amount of tiles in the area.");
    // TODO Load Tile using an abstract resource manager in configManager
    TerrainTile tile = new TerrainTile(configManager.getResourceManager(), areaDefinition.getTileDefinition(index));
    cache.put(index, tile);
    return tile;
  }

  private TerrainTile loadTile(final int index) {
    return this.cache.get(index).orElseGet(() -> createTerrainTile(index));
  }

  void updateLoadedTiles(final int primaryTileIndex) {
    if(loadedTiles.size() == 9 && loadedTiles.get(4).getLocation() == primaryTileIndex) return;
    int length = (this.areaDefinition.getCacheSize() - 1) / 2;
    TerrainTile primaryTile = loadedTiles.stream()
                                         .filter(t -> t.getLocation() == primaryTileIndex)
                                         .findFirst()
                                         .orElseGet(() -> loadTile(primaryTileIndex));
    this.loadedTiles.clear();
    IntStream.rangeClosed(length * -1, length).forEach(i -> {
      TerrainTile tile;
      if(i == 0) tile = primaryTile;
      else tile = this.loadTile(primaryTileIndex + i);
      this.loadedTiles.add(i + length, tile);
    });
  }

  public int getFactor() {
    return areaDefinition.getFactor();
  }

  public ImmutableList<TerrainTile> getLoadedTiles() {
    double[] current = location.get();
    if(!Arrays.equals(lastLocation, current)) {
      lastLocation = current;
      int index = convertToIndex(lastLocation);
      updateLoadedTiles(index);
    }
    return loadedTiles.stream().collect(CustomCollectors.toImmutableList());
  }

  public int getSize() {
    return areaDefinition.getSize();
  }
}
