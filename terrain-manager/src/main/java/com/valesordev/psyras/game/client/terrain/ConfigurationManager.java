/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.valesordev.psyras.game.client.terrain.resources.AbstractResourceManager;
import com.valesordev.psyras.game.client.terrain.resources.DefaultResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by bashburn on 9/19/15.
 */
public class ConfigurationManager {
  private static final Logger LOG = LoggerFactory.getLogger(ConfigurationManager.class);
  private static final ObjectMapper JSON = new ObjectMapper();
  private static ConfigurationManager instance;
  private final Properties properties;
  private final String definitionDirectory;
  private AbstractResourceManager resourceManager;

  private ConfigurationManager() {
    this.properties = new Properties(System.getProperties());
    try {
      this.properties.load(Thread.currentThread()
                                 .getContextClassLoader()
                                 .getResourceAsStream("configuration-manager.properties"));
    } catch(IOException e) {
      LOG.warn("Error loading properties", e);
    }
    resourceManager = new DefaultResourceManager();
    this.definitionDirectory = this.properties.getProperty("psyras.definitionDirectory", "Psyras/TerrainDefinitions");
  }

  public <T> T loadDefinition(final String definitionName, final Class<T> definitionClass) throws DefinitionException {
    JsonNode node = resourceManager.loadJson(definitionDirectory + "/" + definitionName);
    try {
      return JSON.treeToValue(node, definitionClass);
    } catch(JsonProcessingException e) {
      LOG.error("Error converting JSON", e);
      throw new DefinitionException(e);
    }
  }

  public static ConfigurationManager getInstance() {
    if(instance == null) instance = new ConfigurationManager();
    return instance;
  }

  public AbstractResourceManager getResourceManager() {
    return resourceManager;
  }
}
