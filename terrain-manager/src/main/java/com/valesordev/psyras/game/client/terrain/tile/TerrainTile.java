/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.tile;

import com.valesordev.psyras.game.client.terrain.resources.AbstractResourceManager;

/**
 * Created by bashburn on 9/19/15.
 */
public class TerrainTile {
  private int location;
  private float[] heightmap;

  public TerrainTile(final AbstractResourceManager resourceManager, final TerrainTileDefinition tileDefinition) {
    this.location = tileDefinition.getLocation();
    this.heightmap = resourceManager.loadHeightmap(tileDefinition.getName());
    int expectedSize = (tileDefinition.getSize() - 1) + ((tileDefinition.getSize() - 1) * tileDefinition.getSize()) + 1;
    if(heightmap.length != expectedSize) {
      throw new IllegalStateException("Heightmap does not match the size");
    }
  }

  public float[] getHeightmap() {
    return heightmap;
  }

  public int getLocation() {
    return location;
  }
}
