/*
 * Copyright 2015 Valesor Development
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 *
 */

package com.valesordev.psyras.game.client.terrain.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jme3.asset.AssetManager;
import com.jme3.asset.DesktopAssetManager;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;

import java.io.IOException;
import java.net.URL;

/**
 * Created by bashburn on 9/22/15.
 */
public class DefaultResourceManager extends AbstractResourceManager {
  private final static ObjectMapper JSON = new ObjectMapper();
  private final AssetManager assetManager;

  public DefaultResourceManager() {
    URL configUrl = Thread.currentThread().getContextClassLoader().getResource("com.jme3.asset.Desktop.cfg");
    if(configUrl == null) {
      configUrl = AssetManager.class.getResource("Desktop.cfg");
    }
    this.assetManager = new DesktopAssetManager(configUrl);
  }

  @Override
  public float[] loadHeightmap(final String name) {
    Texture texture = assetManager.loadTexture("Psyras/HeightMaps/" + name + "/heightmap.png");
    ImageBasedHeightMap heightMap = new ImageBasedHeightMap(texture.getImage());
    heightMap.load();
    return heightMap.getHeightMap();
  }

  @Override
  public JsonNode loadJson(final String location) {
    try {
      return JSON.readTree(Thread.currentThread().getContextClassLoader().getResource(location + ".json"));
    } catch(IOException e) {
      throw new RuntimeException(e);
    }
  }
}
