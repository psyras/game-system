# Psyras Game System - Terrain Manager

The terrain manager is responsible for managing all of the terrain components. This includes loading and parsing the
various file types needed to define the terrain and managing what terrain is loaded into memory and what terrain is
left on the disk.

## Design

Terrain types that we are looking into:

- Pre-defined areas
- Generated terrain

### Pre-Defined areas

Pre-defined areas will come with a set of heightmap that will be already laid out, as well as any models necessary for
the static scenery. Static scenery can be vegetation or buildings.

### Generated areas

Generated areas is intended to be used in large wilderness areas.

### Terrain Process

The terrain process follows the steps necessary to load and manage the terrain. On initial load, the terrain manager
determines the current location of the player. From that, it is able to load the current area for the player. It will
verify that all the components that are required to present the area to the player. Then all of the required components
for a 3x3 tile area around the player are loaded.
