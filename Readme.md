# Psyras Game System

An open source game system utilizing the [jMonkey Engine 3](http://jmonkeyengine.org) and building a role-playing game
that allows for many different types of customization.
